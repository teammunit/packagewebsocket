<?php

namespace  Notsa\PackageWebSocket\src;
abstract class SocketClient {
    
    /**
     *
     * @var \WebSocket\Client 
     */
    protected $client;
    /**
     *
     * @var PackageManager 
     */
    protected $packageManager;
    
    function __construct($uri, $options = array())
    {
        $this->packageManager = new PackageManager();
        $this->client = new \WebSocket\Client($uri,$options);
        ;
    }
    
    /**
     * 
     * @param BasePackage $package
     * @param bool $shouldRecive
     */
    function sendPackage($package,$shouldRecive = false){
        $this->client->send(get_class($package).':'.JSerialize::encode($package));
        if($shouldRecive){$this->recive();}
    }
    
    public function registerPackageClass($classnames){
       if(is_string($classnames)){
           $classnames = array($classnames);
       }
       foreach($classnames as $class){
           $this->packageManager->register($class,$class);
       }
   }
    
    function recive(){
        try{
            $msg = $this->client->receive();
            list($id,$package) = explode(':',$msg,2);
            $this->callOnMessage($id,$package);
        }catch(\WebSocket\Exception $e){
            
        }
    }
    
    public function callOnMessage($packageid ,$package){
       $packageObj = JSerialize::decode($package, $packageid);
       $this->onMessage($packageObj);
   }
   
   abstract protected function onMessage($packageObj);
}

