<?php

namespace  Notsa\PackageWebSocket\src;

class PackageManager{

	protected $_regPackets = array();

	public function __construct(){
		//$this->_regPackets = new SplObjectStorage;
	}
	
	/**
	 * 
	 * @param int $id
	 * @param string $classname
	 */
	public function register($id,$classname){
		$this->_regPackets[$id] = ($classname);
	}

	/**
	 *
	 * @param int $id
	 * @param string $packet
	 * @param Ratchet\ConnectionInterface $conn
	 */
	public function recive($id,$packet,$conn){
		$classpackage = $this->_regPackets[$id];
		/** @var BasePackage $package */
		$package = JSerialize::decode($packet, $classpackage);
		$package->execute($conn);
	}

}