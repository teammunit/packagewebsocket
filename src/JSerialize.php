<?php
namespace  Notsa\PackageWebSocket\src;

class JSerialize{
		
		public static function encode($obj){
			return json_encode( get_object_vars(($obj)) );
		}
		
		public static function decode($json,$class){
			$obj = new $class;
                        $decode = json_decode($json);
			foreach(get_class_vars($class) as $field=>$value){
                                if(isset($decode->$field)){
                                    $obj->$field = $decode->$field;
                                }
			}
			return $obj;
		}
			
}