<?php
namespace  Notsa\PackageWebSocket\src;

abstract class BasePackage{
        function __construct($params = null)
        {
            if(is_array($params)){
                foreach($params as $field=>$value){
                    $this->$field = $value;
                }
            }
            ;
        }
	abstract function execute(\Ratchet\ConnectionInterface $conn);
}