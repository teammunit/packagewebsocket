<?php

namespace  Notsa\PackageWebSocket\src;
abstract class SocketServer{
    private $host,$path,$port;
    /** @var PacketWebSocketServerMessage Description */
    private $servermessage;
    /** @var \Ratchet\App $app */
    private $app;
    /** @var PackageManager $packageManager*/
    private $packageManager;
    
    /**
     * 
     * @param string $host
     * @param int $port
     * @param string $path exp /test
     * @param array $origin for all allow - array('*')
     */
    function __construct($host,$port,$path,$origin = array('*'))
    {
       $this->host = $host;
       $this->port = $port;
       $this->path = $path;
       $this->packageManager = new PackageManager();
       $this->servermessage  = new PacketWebSocketServerMessage($this);
       $this->app  = new \Ratchet\App($host,$port);
       $this->app->route($path, $this->servermessage, array('*'));
    }
   
   
    public function send(\Ratchet\ConnectionInterface $to,$package){
        $to->send(get_class($package).':'.JSerialize::encode($package));
    }
    
    public function sendToAll($package){
        foreach($this->servermessage->getClients() as $client){
            $this->send($client, $package);
        }
    }
   
    public function registerPackageClass($classnames){
       if(is_string($classnames)){
           $classnames = array($classnames);
       }
       foreach($classnames as $class){
           $this->packageManager->register($class,$class);
       }
   }
   
   public function run(){
       $this->app->run();
   }
   
   public function callOnMessage(\Ratchet\ConnectionInterface $from, $packageid ,$package){
       $packageObj = JSerialize::decode($package, $packageid);
       $packageObj->execute($from);
       $this->onMessage($from, $packageObj);
   }
   
   abstract protected function onMessage(\Ratchet\ConnectionInterface $from,$package);
}