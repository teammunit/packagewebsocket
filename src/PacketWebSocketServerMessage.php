<?php

namespace  Notsa\PackageWebSocket\src;
use Ratchet\MessageComponentInterface;

class PacketWebSocketServerMessage implements MessageComponentInterface {
    protected $clients;
    /** @var Notsa\PackageWebSocket\src\SocketServer */
    protected $owner;

    
    public function __construct($owner) {
        $this->clients = new \SplObjectStorage;
        $this->owner   = $owner;
    }

    public function onOpen(\Ratchet\ConnectionInterface $conn) {
        $this->clients->attach($conn);
    }

    public function onMessage(\Ratchet\ConnectionInterface $from, $msg) {
        list($id,$package) = explode(':',$msg,2);
        $this->owner->callOnMessage($from,$id,$package);
    }

    public function onClose(\Ratchet\ConnectionInterface $conn) {
        $this->clients->detach($conn);
    }

    public function onError(\Ratchet\ConnectionInterface $conn, \Exception $e) {
        $conn->close();
    }
    
    public function getClients(){
        return $this->clients;
    }
    
}